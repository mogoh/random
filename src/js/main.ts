import { Model } from "./Model.js";
import { View } from "./View.js";

let model = new Model();
await model.async_constructor();
model.new();
new View(model);
