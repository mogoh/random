import { Model } from "./Model.js";


export class View {
    app = document.createElement("div");
    next_button = document.createElement("button");
    content = document.createElement("div");
    model: Model;

    constructor(model: Model) {
        this.model = model;
        this.app.classList.add("app");
        this.content.classList.add("content");
        this.next_button.innerText = "Noch einmal";
        this.app.append(this.content, this.next_button);

        let center = document.createElement("div");
        center.classList.add("center");
        this.content.append(center);

        document.body.append(this.app);
        this.render_content();

        this.next_button.onclick = () => { this.next(); };
    }

    next() {
        this.model.new();
        this.render_content();
    }

    render_content() {
        this.content.innerHTML = `
            <div class="center">
            <h1>Zufälliges</h1>
            <h2>Persönliches</h2>
            <dl>
            <dt>Geschlecht</dt>
            <dd>${this.model.generated.get("de/person/gender.csv")}</dd>
            <dt>Ethnie</dt>
            <dd>${this.model.generated.get("de_ethnos")}</dd>
            <dt>Augefarbe</dt>
            <dd>${this.model.generated.get("de/person/eye_color.txt")}</dd>
            <dt>Hautfarbe</dt>
            <dd>${this.model.generated.get("de/person/skin_color.txt")}</dd>
            <dt>Haarfarbe</dt>
            <dd>${this.model.generated.get("de/person/hair_color.txt")}</dd>
            <dt>Adjektive</dt>
            <dd>${this.model.generated.get("de_adjectives1")}, ${this.model.generated.get("de_adjectives2")}, ${this.model.generated.get("de_adjectives3")}</dd>
            </dl>
            <h2>Namen</h2>
            <dl>
            <dt>Nachname, DE</dt>
            <dd>${this.model.generated.get("de/names/lastname.txt")}</dd>
            <dt>Vorname, DE, Weiblich</dt>
            <dd>${this.model.generated.get("de/names/firstname_female.txt")}</dd>
            <dt>Vorname, DE, Männlich</dt>
            <dd>${this.model.generated.get("de/names/firstname_male.txt")}</dd>
            <dt>Vorname, EN, Weiblich</dt>
            <dd>${this.model.generated.get("en/names/firstname_female.txt")}</dd>
            <dt>Vorname, EN, Männlich</dt>
            <dd>${this.model.generated.get("en/names/firstname_male.txt")}</dd>
            <dt>Fantasy</dt>
            <dd>${this.model.generated.get("int/fantasy.txt")}</dd>
            <dt>Mythologisches (irdisch)</dt>
            <dd>${this.model.generated.get("de/mythology/all.txt")}</dd>
            </dl>
            <h2>Dinge</h2>
            <dl>
            <dt>Farbe</dt>
            <dd>${this.model.generated.get("de/things/colors.txt")}</dd>
            <dt>Sternbild</dt>
            <dd>${this.model.generated.get("de/things/constellations.txt")}</dd>
            <dt>Edelstein</dt>
            <dd>${this.model.generated.get("de/things/gemstone.txt")}</dd>
            <dt>Heilpflanze</dt>
            <dd>${this.model.generated.get("de/things/medical_plant.txt")}</dd>
            </dl>
            <h2>Orte</h2>
            <dl>
            <dt>Ort, DE</dt>
            <dd>${this.model.generated.get("de/places/germany.txt")}</dd>
            <dt>Millionenstadt</dt>
            <dd>${this.model.generated.get("de/places/megacities.txt")}</dd>
            <dt>Ort in der Bibel</dt>
            <dd>${this.model.generated.get("de/places/places_in_the_bible.txt")}</dd>
            <dt>Stern</dt>
            <dd>${this.model.generated.get("de/places/stars.txt")}</dd>
            <dt>Staat der USA</dt>
            <dd>${this.model.generated.get("de/places/states_of_usa.txt")}</dd>
            <dt>Staat</dt>
            <dd>${this.model.generated.get("de/places/states.txt")}</dd>
            </dl>
            <h2>Generiertes</h2>
            <dl>
            <dt>Firma</dt>
            <dd>${this.model.generated.get("en_concern")}</dd>
            </dl>
            <dl>
            <dt>Nachname, DE</dt>
            <dd>${this.model.generated.get("de/names/lastname_generated.txt")}</dd>
            <dt>Vorname, DE, Unisex</dt>
            <dd>${this.model.generated.get("de/names/firstname_unisex_generted.txt")}</dd>
            <dt>Vorname, DE, Weiblich</dt>
            <dd>${this.model.generated.get("de/names/firstname_female_generated.txt")}</dd>
            <dt>Vorname, DE, Männlich</dt>
            <dd>${this.model.generated.get("de/names/firstname_male_generated.txt")}</dd>
            <dt>Nachname, EN</dt>
            <dd>${this.model.generated.get("en/names/lastname_generated.txt")}</dd>
            <dt>Vorname, EN, Unisex</dt>
            <dd>${this.model.generated.get("en/names/firstname_unisex.txt")}</dd>
            <dt>Vorname, EN, Weiblich</dt>
            <dd>${this.model.generated.get("en/names/firstname_female_generated.txt")}</dd>
            <dt>Vorname, EN, Männlich</dt>
            <dd>${this.model.generated.get("en/names/firstname_male_generated.txt")}</dd>
            <dt>Fantasy</dt>
            <dd>${this.model.generated.get("int/fantasy.txt")}</dd>
            </dl>
            </div>
        `;
    }
}
