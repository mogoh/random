import { PRNG } from "./PRNG.js";

const PATH = "data/";
const FILES = [
    "de/ethnos/africa.txt",
    "de/ethnos/america.txt",
    "de/ethnos/asia.txt",
    "de/ethnos/europe.txt",
    "de/ethnos/oceania.txt",

    "de/letters/arabic_alphabet.txt",
    "de/letters/greek_alphabet.txt",
    "de/letters/hebrew_alphabet.txt",

    "de/mythology/all.txt",

    "de/names/firstname_female_generated.txt",
    "de/names/firstname_female.txt",
    "de/names/firstname_male_generated.txt",
    "de/names/firstname_male.txt",
    "de/names/firstname_unisex_generted.txt",
    "de/names/lastname_generated.txt",
    "de/names/lastname.txt",

    "de/person/eye_color.txt",
    "de/person/gender.csv",
    "de/person/hair_color.txt",
    "de/person/skin_color.txt",

    "de/places/germany.txt",
    "de/places/megacities.txt",
    "de/places/places_in_the_bible.txt",
    "de/places/stars.txt",
    "de/places/states_of_usa.txt",
    "de/places/states.txt",

    "de/things/colors.txt",
    "de/things/constellations.txt",
    "de/things/gemstone.txt",
    "de/things/medical_plant.txt",

    "de/words/adjectives.txt",

    "en/names/firstname_female_generated.txt",
    "en/names/firstname_female.txt",
    "en/names/firstname_male_generated.txt",
    "en/names/firstname_male.txt",
    "en/names/firstname_unisex.txt",
    "en/names/lastname_generated.txt",

    "en/concern_part.txt",

    "int/fantasy.txt",
];

function parse_csv(table: string): any[] {
    return table.split("\n").map((line) =>
        line.split(",").map((v, i) => {
            return i === 0 ? v : Number(v);
        })
    );
}

export class Model {
    data = new Map<string, string[]>();
    data_fractions = new Map<string, [string, number][]>();
    prng = new PRNG();
    generated = new Map<string, string>();

    constructor() { }

    async async_constructor() {
        for (let file of FILES) {
            if (file.endsWith("csv")) {
                let response = await fetch(PATH + file);
                let text = await response.text();
                let data = parse_csv(text);
                this.data_fractions.set(file, data);
            } else if (file.endsWith("txt")) {
                let response = await fetch(PATH + file);
                let text = await response.text();
                let data = text.split("\n");
                this.data.set(file, data);
            }
        }
    }

    new() {
        for (let file of FILES) {
            if (file.endsWith("csv")) {
                this.generated.set(file, this.prng.fractionChoice(this.data_fractions.get(file)!));
            } else if (file.endsWith("part.txt")) {
                let arabic = this.data.get("de/letters/arabic_alphabet.txt")!;
                let greek = this.data.get("de/letters/greek_alphabet.txt")!;
                let hebrew = this.data.get("de/letters/hebrew_alphabet.txt")!;
                let letters = [...arabic, ...greek, ...hebrew];
                let part1 = this.prng.choice(letters);
                let part2 = this.prng.choice(this.data.get(file)!);
                this.generated.set("en_concern", part1 + " " + part2);
            } else if (file.endsWith("txt")) {
                this.generated.set(file, this.prng.choice(this.data.get(file)!));
            }
        }
        let ethnos_africa = this.data.get("de/ethnos/africa.txt")!;
        let ethnos_america = this.data.get("de/ethnos/america.txt")!;
        let ethnos_asia = this.data.get("de/ethnos/asia.txt")!;
        let ethnos_europe = this.data.get("de/ethnos/europe.txt")!;
        let ethnos_oceania = this.data.get("de/ethnos/oceania.txt")!;
        let ethnos = [...ethnos_africa, ...ethnos_america, ...ethnos_asia, ...ethnos_europe, ...ethnos_oceania];
        let ethnos_choice = this.prng.choice(ethnos);
        this.generated.set("de_ethnos", ethnos_choice);
        this.generated.set("de_adjectives1", this.prng.choice(this.data.get("de/words/adjectives.txt")!));
        this.generated.set("de_adjectives2", this.prng.choice(this.data.get("de/words/adjectives.txt")!));
        this.generated.set("de_adjectives3", this.prng.choice(this.data.get("de/words/adjectives.txt")!));
    }
}
