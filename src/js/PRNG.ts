export class PRNG {
    private _seed: number;

    constructor(seed?: number) {
        if (seed !== undefined) {
            this._seed = seed;
        } else {
            this._seed = this.randomSeed();
        }
    }

    set seed(s: number) {
        this._seed = s;
    }

    /**
     * Generates a random 32bit integer.
     */
    randomSeed(): number {
        return Math.floor(2 ** 32 * Math.random());
    }

    /**
     * Source: https://github.com/bryc/code/blob/master/jshash/PRNGs.md
     */
    mulberry32(): number {
        this._seed |= 0;
        this._seed = (this._seed + 0x6d2b79f5) | 0;
        let t = Math.imul(this._seed ^ (this._seed >>> 15), 1 | this._seed);
        t = (t + Math.imul(t ^ (t >>> 7), 61 | t)) ^ t;
        return ((t ^ (t >>> 14)) >>> 0) / 4294967296;
    }

    nextNumber(): number {
        return this.mulberry32();
    }

    /**
     * @param min minimum (inclusive)
     * @param max maximum (exclusive)
     */
    nextInt(min: number, max: number): number {
        return Math.floor(this.nextNumber() * (max - min)) + min;
    }

    choice<T>(a: Array<T>): T {
        let choice = this.nextInt(0, a.length);
        return a[choice];
    }

    fractionChoice<T>(a: Array<[T, number]>): T {
        let sum = a.reduce((acc, current) => acc + current[1], 0);
        let choice = this.nextInt(1, sum + 1);
        let i = 0;
        let acc = 0;
        while (acc < choice) {
            acc += a[i][1];
            i += 1;
        }
        return a[i - 1][0];
    }
}
