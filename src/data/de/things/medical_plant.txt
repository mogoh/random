Abendländischer Lebensbaum
Acker-Minze
Acker-Schachtelhalm
Acker-Vergissmeinnicht
Acker-Winde
Acker-Witwenblume
Acker-Gauchheil
Ackerlauch
Acker-Schöterich
Afrikanische Teufelskralle
Afrikanischer Faulbaum
Ähriges Christophskraut
Ajowan
Alexandrinische Senna
Alpen-Ampfer
Alpen-Mutterwurz
Amerikanische Kermesbeere
Amerikanischer Kalmus
Angenehme Akelei
Anis
Kulturapfel
Aquilegia nikolicii
Arnika
Aromata
Artischocke
Arznei-Engelwurz
Augentrost
Aztekisches Süßkraut
Ballonerbse
Balsam-Tanne
Bärlauch
Bärwurz
Basilikum
Beifuß
Beinwell
Benediktenkraut
Berg-Johanniskraut
Besenginster
Betonica betoniciflora
Betonica haussknechtii
Betonica nivea
Betonica orientalis
Betonica ossetica
Betonica scardica
Birkenblätter
Bischofskraut
Bittere Schafgarbe
Bittermelone
Blauer Eisenhut
Blauer Eukalyptus
Blutwurz
Bockshornklee
Boldo
Borretsch
Borstige Taigawurzel
Boswellia sacra
Breitwegerich
Cardy
Celastrus paniculatus
Cetraria islandica
Ceylon-Zimtbaum
Chayote
Chinesische Engelwurz
Chinesische Jujube
Chinesisches Spaltkörbchen
Christusdorn
Cootamundra-Akazie
Damiana
Dichtblüten-Betonie
Diptam
Diptam-Dost
Dornige Hauhechel
Dreilappige Papau
Duftveilchen
Eberraute
Echte Aloe
Echte Bärentraube
Echte Betonie
Echte Brunnenkresse
Echte Feige
Echte Kamille
Echter Alant
Echter Baldrian
Echter Beinwell
Echter Ehrenpreis
Echter Eibisch
Echter Galgant
Echter Hopfen
Echter Koriander
Echter Lavendel
Echter Lorbeer
Echter Salbei
Echter Thymian
Echtes Eisenkraut
Echtes Herzgespann
Echtes Johanniskraut
Echtes Labkraut
Echtes Leinkraut
Echtes Mädesüß
Edel-Gamander
Eibischwurzel
Estragon
Europäische Stechpalme
Faulbaum
Fenchel
Frauenminze
Frühlings-Adonisröschen
Frühlingslichtblume
Futter-Beinwell
Gagelstrauch
Gänseblümchen
Gänsefingerkraut
Gartenkürbis
Gebirgs-Rose
Geflecktes Lungenkraut
Geißraute
Gemeine Akelei
Gemeine Esche
Gemeine Nachtkerze
Gemeine Pfingstrose
Gemeine Schafgarbe
Gemeiner Augentrost
Gemeiner Efeu
Gemeiner Lein
Gemeiner Odermennig
Gemeiner Wacholder
Gemeiner Wirbeldost
Gerber-Akazie
Geschwänzte Brennnessel
Gewöhnliche Goldrute
Gewöhnliche Haselwurz
Gewöhnliche Kuhschelle
Gewöhnliche Osterluzei
Gewöhnliche Rosskastanie
Gewöhnliche Vogelmiere
Gewöhnlicher Andorn
Gewöhnlicher Blutweiderich
Gewöhnliches Hirtentäschel
Gewürzlilie
Giersch
Gift-Lattich
Ginkgo
Ginseng
Golpar
Griechischer Oregano
Großblütige Betonie
Große Brennnessel
Große Sterndolde
Großer Wiesenknopf
Großes Flohkraut
Großfrüchtige Moosbeere
Grüne Minze
Gummiarabikumbaum
Gynostemma pentaphyllum
Saat-Hafer
Hain-Minze
Hanf
Hänge-Birke
Hawaiianische Holzrose
Heidelbeere
Herbstzeitlose
Hintonia latiflora
Hopfenzapfen
Huflattich
Igel-Stachelbart
Indianertabak
Indische Kostuswurzel
Indische Narde
Indischer Hanf
Indischer Wassernabel
Indisches Basilikum
Ingwer
Juckbohne
Kalifornischer Mohn
Kalmus
Kalumba
Karotte
Katappenbaum
Katzenkralle
Kinkéliba
Kitaibel-Akelei
Kleeulme
Kleine Bibernelle
Kleine Braunelle
Kleine Brennnessel
Kleiner Wiesenknopf
Knoblauch
Krim-Pfingstrose
Kubeben-Pfeffer
Echter Kümmel
Kurkuma
Langer Koriander
Lichtnussbaum
Liebstöckel
Gewöhnlicher Löwenzahn
Luo Han Guo
Maiglöckchen
Majoran
Mariendistel
Maulbeerblättrige Brennnessel
Melastoma malabathricum
Mexikanischer Blattpfeffer
Mexikanischer Drüsengänsefuß
Mitsuba
Moltebeere
Mönchspfeffer
Moor-Birke
Moschus-Malve
Muskatellersalbei
Muskatnussbaum
Mutterkraut
Myrte
Nara
Okoubaka aubrevillei
Olivenbaum
Orangerotes Habichtskraut
Oregano
Paeonia peregrina
Passiflora incarnata
Petersilie
Pfefferminze
Pfeilblättrige Balsamwurzel
Picralima nitida
Pillen-Brennnessel
Plantago ovata
Platostoma palustre
Ptisane
Purpur-Sonnenhut
Purpurrote Taubnessel
Quassia amara
Quendelblättrige Bergminze
Quirl-Malve
Quitte
Rainfarn
Rangunschlinger
Rauschminze
Rhapontik-Rhabarber
Ringelblume
Röhricht-Brennnessel
Römische Kamille
Rosa damascena
Rosenwurz
Rosmarin
Roter Chinarindenbaum
Roter Fingerhut
Rotwurzel-Salbei
Ruderal-Hanf
Salbei-Gamander
Sanddorn
Schlafmohn
Schlehdorn
Schmalblättriger Sonnenhut
Schneerose
Schöllkraut
Schwarze Tollkirsche
Schwarzer Holunder
Schwarzer Senf
Schwarzes Bilsenkraut
Sibirische Engelwurz
Sibirische Hanfnessel
Silberdistel
Silphium
Smilax officinalis
Sommer-Bohnenkraut
Sommerlinde
Späte Betonie
Spitzwegerich
Stephanskraut
Steppenraute
Stevia rebaudiana
Strahlenlose Kamille
Strand-Beifuß
Strauch-Rosskastanie
Südseemyrte
Tee
Trauben-Silberkerze
Vielblütiger Knöterich
Virginische Zaubernuss
Vogelbeere
Wald-Erdbeere
Waldmeister
Warburgia salutaris
Wasserminze
Weg-Rauke
Weihrauch
Weiße Meerzwiebel
Wermutkraut
Wilde Malve
Wunderbaum
Wurmsamen
Yohimbe
Ysop
Zahnbürstenbaum
Zaubernuss
Zimbelkraut
Zimt-Himbeere
Zitronen-Thymian
Zitronenmelisse
Zitwerwurzel